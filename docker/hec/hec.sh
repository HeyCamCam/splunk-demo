#!/bin/bash

count=0

while [ true ]
do
	curl -k https://splunk:8088/services/collector/event -H "Authorization: Splunk abcd1234" -d '{"event": "Hello from the bash container - '${count}'"}'
	count=$((count+1))
	sleep 2
done
