# Splunk Enterprise Demonstration  

![image info](./pictures/splunk.jpg)  

Splunk Enterprise is the data collection, indexing, and visualization engine for operational intelligence. The source of the data can be event logs, web logs, live application logs, network feeds, system metrics, change monitoring, message queues, archive files, and so on. You can also browse [Splunkbase](https://splunkbase.splunk.com/) to find applications that provide data knowledge, reports, and dashboards.

In general, data sources are grouped into the following categories:  
![Sources](./pictures/sources.png)  

---
### **Architecture**

![image info](./pictures/architecture.jpg)

Data Stream Processor
![Data Sources](./pictures/data-sources.png)
---
### **Terminology**
+ [Universal Forwarder](https://docs.splunk.com/Documentation/Forwarder/9.1.2/Forwarder/Abouttheuniversalforwarder): Component that collects logs and sends them to the indexer. There can be multiple forwarders in a Splunk deployment.
+ [Index](https://docs.splunk.com/Splexicon:Index): The repository for data. When the Splunk platform indexes raw data, it transforms the data into searchable events.  
There are two types of indexes:
  - Events indexes. Events indexes are the default type of index. They can hold any type of data.
  - Metrics indexes. Metrics indexes hold only metric data. 
+ [Indexer](https://docs.splunk.com/Splexicon:Indexer): A Splunk Enterprise instance that indexes data, transforming raw data into events and placing the results into an index. It also searches the indexed data in response to search requests.
+ [Search Head](https://docs.splunk.com/Splexicon:Searchhead): In a distributed search environment, a Splunk Enterprise instance that handles search management functions, directing search requests to a set of search peers and then merging the results back to the user. A Splunk Enterprise instance can function as both a search head and a search peer. A search head that performs only searching, and not any indexing, is referred to as a dedicated search head.
+ [Metrics](https://docs.splunk.com/Documentation/Splunk/9.1.2/Metrics/Overview): A feature for system administrators, IT, and service engineers that focuses on collecting, investigating, monitoring, and sharing metrics from your technology infrastructure, security systems, and business applications in real time. In the Splunk platform, you use metric indexes to store metrics data. This index type is optimized for the storage and retrieval of metric data.  
+ [Search Processing Language (SPL)](https://docs.splunk.com/Splexicon:SPL): SPL encompasses all the search commands and their functions, arguments, and clauses. Its syntax was originally based on the Unix pipeline and SQL. The scope of SPL includes data searching, filtering, modification, manipulation, insertion, and deletion.
+ [Search & Reporting application (Search app)](https://docs.splunk.com/Splexicon:Searchapp): The common name for the Search & Reporting app, which is the default interface for searching and analyzing IT data in Splunk Enterprise. You can use the Search & Reporting app to index data, manage knowledge objects, build reports, configure alerts, and create dashboards.
+ [HTTP Event Collector (HEC)](https://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector): A fast way to send data to Splunk using token-based authentication over HTTP and HTTPS protocols. You generate a token and then configure a logging library or HTTP client with the token to send data to HEC in a specific format.

---
### **Ports**
![image info](./pictures/ports.png)  
---
### **Licensing**
![image info](./pictures/pricing.png)  
There are two options for licensing Splunk:
  - Ingest pricing: Cost is calculated based on how many GB per day of data you ingest into Splunk. Benefit is if your ingestion size is stable, you have unlimited search activity for the price.
  - Workload pricing: Cost is calculated in regard to the intensity of your search activity rather than your data volume. Splunk uses Central Processing Units to measure your search activity. Benefit is that you have flexibility in crafting low cost or affordable searches regardless of the amount of data you're ingesting.  
---
### **Helpful Links**
- __[Splunk Blog](https://www.splunk.com/en_us/blog/author/admin.html)__ - The official Splunk blog for staying up to date on news from Splunk.
- __[Splunk Documentation](https://docs.splunk.com/Documentation)__ - Official Splunk documentation.
- __[Reference Guide](https://www.splunk.com/en_us/resources/splunk-quick-reference-guide.html)__ - Resource for understanding common Splunk components.
- __[Common Search Commands](https://www.stationx.net/splunk-cheat-sheet/)__ - Resource to help build searches.
- __[List of Windows Security Log Events](https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/default.aspx)__ - Resource that keeps track of Windows event IDs.
- __[.Conf Splunk Conference](https://conf.splunk.com/)__ - Splunk conference information.
- __[Splunk Events](https://www.splunk.com/en_us/about-us/events.html)__ - Follow various Splunk conference dates.
- __[Splunk University](https://conf.splunk.com/splunk-university.html)__ - Pre-conference training.
